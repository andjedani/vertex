FROM golang:stretch
# RUN apt-get update && apt-get -y install apt-utils && apt-get install -y unzip && apt-get install wget && apt-get install -y curl 
# RUN apt-get clean
RUN go get -u github.com/golang/dep/cmd/dep

RUN export GOBIN=$GOPATH/bin && mkdir -p $GOPATH/src/vertex
ADD . $GOPATH/src/vertex
WORKDIR $GOPATH/src/vertex
RUN cp ./.vertex.yaml.sample ~/.vertex.yaml
RUN make build 
EXPOSE 1170 


RUN mkdir -p /bin/vertex
RUN export PATH="$PATH:/bin/vertex"
 
ADD vertex /bin/vertex

WORKDIR /bin/vertex
# 
# EXPOSE 61613
# 
ENTRYPOINT ./vertex serve

